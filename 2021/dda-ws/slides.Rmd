---
title: "Poster pitch for DDA 2021"
output:
  xaringan::moon_reader:
    seal: false # Remove title slide
    lib_dir: libs
    css: "xaringan-themer.css"
    nature:
      slideNumberFormat: ""
      ratio: "16:9"
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

class: middle

<!-- Knit from document directory -->

# Science is largely done in a non-reproducible and closed way

<hr>
<br>

### Shifting that research culture, one course at a time

.footnote[[r-cubed.rostools.org](https://r-cubed.rostools.org/)]

```{r setup, include=FALSE}
library(xaringanthemer)
```

```{r xaringan-themer, include=FALSE}
style_mono_dark(
    white_color = "#FFFFFF",
    black_color = "#383838",
    base_font_size = "23px",
    header_font_google = google_font("Merriweather Sans", "300")
)
```

<!-- 
# Spoken presentation: 

Words: / 100-150

I wanted to enter this pitch with a slightly provocative but largely true title.
Is provocative enough? Maybe...

When making this presentation, I thought about presenting some of my diabetes
research. But, while I believe the work is important, its not urgent. This on
the other hand, is both important and urgent.

This work that myself and several others are doing is to shift the
research culture towards being more open, rigorous, and reproducible. We do this
by training and educating researchers in courses taught through the DDA.

We teach researchers how they can use R and other modern
computing tools and skills to be better, faster, and more productive scientists
when doing their data analysis.

Interested in learning more, come to poster ## or talk to me anytime!

-->
