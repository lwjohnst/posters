---
title: "Research Operations"
subtitle: Improving research through education and building better social, organizational, and technical infrastructures
output:
  drposter::drposter_poster:
    self_contained: true
    css: custom.css
editor:
  markdown:
    wrap: 72
    canonical: true
---

#  {.col-2}

## Mission

Running activities, events, and projects that center on "meta-research",
open science, coding and reproducibility, software development, project
/ organization management, and effective collaboration to achieve the
vision

## Courses in Reproducible Research

-   Main site: `r-cubed.rostools.org`
-   Introductory: `r-cubed-intro.rostools.org`
-   Intermediate: `r-cubed-intermediate.rostools.org`
-   Advanced: `r-cubed-advanced.rostools.org`

![March 2023 Intro course](images/r3-intro.jpeg){width="90%"}

## Coding Club

<!-- ![](https://www.r-project.org/logo/Rlogo.png){width="40%"} -->

-   **What**: Learning about R in an informal setting where one person
    codes and another person explains
-   **Where**: Virtually, see `coding-club.rostools.org` website for
    details
-   **When**: At the start of every month, for an hour

![Artwork by @allison_horst.](images/quarto-workflow.png){width="90%"}

<!-- ![](https://www.r-project.org/logo/Rlogo.png){width="40%"} -->

<!-- ![](https://tidyverse.tidyverse.org/logo.png){width="35%"} -->

<!-- ![](https://quarto.org/quarto.png){width="80%"} -->

#  {.col-2}

## Steno Common Docs

Building a collaborative, documentation-first research culture through
shared documents for onboarding, common practices, and operating
procedures. Found at `steno-aarhus.github.io/research/`

## Seedcase Project

**Aim**: Easier managing, sharing, and accessing health data by making
user-friendly software to automate or streamline these task. Found at
`steno-aarhus.github.io/seedcase-project`

![](https://steno-aarhus.github.io/seedcase-project/design/images/context.png){width="80%"}

## Projects in Action

### UK Biobank

-   Manage through Trello
-   Community building
-   Shared workflows
-   Code reviews

### DST Register

-   Manage through Trello
-   Software to speed up analysis on register data
-   Code reviews

### 

![Trello tracking board](images/trello-ukbiobank.png){width="80%"}

#  {.col-2}

## Future Plans

-   Course: *Reproducible Research in R: Within server environments like
    Denmark Statisics and UK Biobank*
-   Data Operations (DataOps) and Pipelines
-   Incorporating small software projects in PhD projects?
-   Have any ideas? Contact Luke!

#  {.col-2}

## Contact {.theme-minimalist}

| Luke Johnston
| Email: [lwjohnst\@clin.au.dk](mailto:lwjohnst@clin.au.dk){.email}
| License: CC-BY 4.0
| **NOTE**: This "space" is still being refined!
